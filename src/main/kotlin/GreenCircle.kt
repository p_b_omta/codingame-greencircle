import java.util.Scanner

import kotlin.collections.List
import kotlin.math.*

internal const val TRAINING = 0
internal const val CODING = 1
internal const val DAILY_ROUTINE = 2
internal const val TASK_PRIORITIZATION = 3
internal const val ARCHITECTURE_STUDY = 4
internal const val CONTINUOUS_INTEGRATION = 5
internal const val CODE_REVIEW = 6
internal const val REFACTORING = 7
internal const val BONUS = 8
internal const val TECH_DEBT = 9
internal const val nr_skills = 8
internal const val invalid_desk = nr_skills

data class Phase(private val phaseAsText: String) {
    enum class Phases { Move, GiveCard, ThrowCard, PlayCard, Release, Invalid }

    private val phase = when(phaseAsText) {
        "MOVE" -> Phases.Move
        "GIVE_CARD" -> Phases.GiveCard
        "THROW_CARD" -> Phases.ThrowCard
        "PLAY_CARD" -> Phases.PlayCard
        "RELEASE" -> Phases.Release
        else -> Phases.Invalid
    }

    val isMovePhase
    get() = phase == Phases.Move

    val isThrowCardPhase
    get() = phase == Phases.ThrowCard

    val isPlayPhase
    get() = phase == Phases.PlayCard

    val isReleasePhase
    get() = phase == Phases.Release
}

data class Application(
        val id: Int,
        private val skillsNeeded: List<Int>
) {
    companion object {
        fun parse(input: Scanner): Application {
            val objectType = input.next()
            assert(objectType == "APPLICATION")
            return Application(input.nextInt(), List<Int>(nr_skills) { input.nextInt() })
        }
    }

    // Returns 0 for no tech debt, -1 for each tech debt card and -999 if impossible
    fun score(cards: StackOfCards): Int {
        var bonusSkillsNeeded = 0
        for (n in 0 until nr_skills) {
            bonusSkillsNeeded += max(0, skillsNeeded[n] - 2 * cards.cardsCount[n])
        }
        val shoddySkillsNeeded = max(0, bonusSkillsNeeded - cards.bonusCardsCount)
        if (cards.shoddySkills < shoddySkillsNeeded) return -999
        return -1 * shoddySkillsNeeded
    }

    fun requires(skill: Int): Int {
        return skillsNeeded[skill]
    }
}

data class Player(
        val location: Int,// id of the zone in which the player is located
        val score: Int,
        val permanentDailyRoutineCards: Int, // number of DAILY_ROUTINE the player has played. It allows them to take cards from the adjacent zones
        val permanentArchitectureStudyCards: Int // number of ARCHITECTURE_STUDY the player has played. It allows them to draw more cards
) {
    companion object {
        fun parse(input: Scanner): Player {
            return Player(input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt())
        }
    }
    val busyWithLastApp: Boolean
    get() = score == 4

    val wantsDailyRoutine: Boolean
    get() = score == 0 && permanentDailyRoutineCards < 1 // Only for the first application and max 1 card.

    val wantsArchitectureStudy: Boolean
    get() = score == 4 // Quite useful for the last app, no limit

    val grabDistance: Int
    get() = permanentDailyRoutineCards

    fun duplicateNewLocation(newLocation: Int): Player {
        return Player(newLocation, score, permanentDailyRoutineCards, permanentArchitectureStudyCards)
    }
}

data class StackOfCards(
        // Skill cards + bonus cards + tech debt cards
        val cardsCount: List<Int> = List<Int>(nr_skills + 2) { 0 }
) {
    init {
        assert(cardsCount.size == 10)
    }

    val bonusCardsCount: Int
    get() = cardsCount[BONUS]

    val technicalDebtCardsCount: Int
    get() = cardsCount[TECH_DEBT]

    private val totalSkillCards: Int
    get() = cardsCount.slice(0 until nr_skills).sum()

    val shoddySkills: Int
    get() = 2 * totalSkillCards + bonusCardsCount

    fun cardCount(deskId: Int) = cardsCount[deskId]

    companion object {
        fun parse(input: Scanner): StackOfCards {
            return StackOfCards(List<Int>(nr_skills + 2) { input.nextInt() })
        }
    }

    fun duplicateWithTwoBonusCardsLess(): StackOfCards {
        assert(bonusCardsCount >= 2)
        val newSkillCardsCount = cardsCount.toMutableList()
        newSkillCardsCount[BONUS] = max(0, newSkillCardsCount[BONUS] - 2)
        return StackOfCards(newSkillCardsCount.toList())
    }

    fun duplicateWithAdditionalCard(cardIdx: Int): StackOfCards {
        assert(cardIdx >= 0)
        assert(cardIdx < nr_skills + 2)
        val newSkillCardsCount = cardsCount.toMutableList()
        newSkillCardsCount[cardIdx] += 1
        return StackOfCards(newSkillCardsCount.toList())
    }

    fun allDuplicatesOneCardLess(): List<StackOfCards> {
        val result = mutableListOf<StackOfCards>()
        for (n in 0 until nr_skills + 1) {
            if (cardsCount[n] > 0) {
                val newCardsCount = cardsCount.toMutableList()
                newCardsCount[n] -= 1
                result.add(StackOfCards(newCardsCount))
            }
        }
        if (result.isEmpty()) {
            result.add(duplicateWithAdditionalCard(TECH_DEBT))
        }
        return result
    }

    operator fun plus(rhs: StackOfCards): StackOfCards {
        return StackOfCards(cardsCount.zip(rhs.cardsCount) { a, b -> a + b })
    }

    operator fun minus(rhs: StackOfCards): StackOfCards {
        return StackOfCards(cardsCount.zip(rhs.cardsCount) { a, b -> a - b })
    }

    operator fun minus(player: Player): StackOfCards {
        val newSkillCardCount = cardsCount.toMutableList()
        newSkillCardCount[DAILY_ROUTINE] -= player.permanentDailyRoutineCards
        newSkillCardCount[ARCHITECTURE_STUDY] -= player.permanentArchitectureStudyCards
        return StackOfCards(newSkillCardCount.toList())
    }
}

val initial_board_stack = StackOfCards(listOf(5, 5, 5, 5, 5, 5, 5, 5, 36, 100))

// Returns the application to release with the best score. null if we can't release anything with the given cards.
fun findBestApplicationToRelease(applications: List<Application>, cards: StackOfCards): Pair<Application?, Int> {
    var best: Application? = null
    var bestScore = -900 // FIXME: make some constant out of this
    applications.forEach {
        val score = it.score(cards)
        if (score > bestScore) {
            best = it
            bestScore = score
        }
    }
    return Pair<Application?, Int>(best, bestScore)
}

fun deskDistance(a: Int, b: Int): Int {
    if (a == -1 || b == -1) return 999

    val aa = min(a, b)
    val bb = max(a, b)
    return min(bb - aa, aa + nr_skills + 1 - bb)
}

fun nextDesk(n: Int): Int {
    var next = n + 1
    if (next >= nr_skills) {
        return next - nr_skills
    }
    return next
}

fun findBestMoveCommand(
    player: Player,
    playerCards: StackOfCards,
    automaticPlayerCards: StackOfCards,
    restPlayerCards: StackOfCards,
    opponent: Player,
    totalOpponentCards: StackOfCards,
    applications: List<Application>,
    boardStack: StackOfCards
): String {
    var startLocation = if(player.location == -1) nr_skills - 1 else player.location
    var newLocation = nextDesk(startLocation)
    var bestLocation = newLocation
    var bestCardToGrab = newLocation
    var bestScore = -999
    var movePenalty = 0
    while (newLocation != startLocation) {
        if (deskDistance(newLocation, opponent.location) <= 1) {
            newLocation = nextDesk(newLocation)
            movePenalty += 1
            continue
        }

        var possiblePlayerCards = mutableListOf(playerCards)
        if (newLocation < player.location) {
            possiblePlayerCards.clear()
            val step1 = playerCards.allDuplicatesOneCardLess()
            for (p in step1) {
                possiblePlayerCards.addAll(p.allDuplicatesOneCardLess())
            }
        }
        for (pCards in possiblePlayerCards) {
            val (score, cardToGrab) = findBestCardToGrab(
                player.duplicateNewLocation(newLocation),
                pCards,
                automaticPlayerCards,
                restPlayerCards,
                opponent,
                totalOpponentCards,
                applications,
                boardStack
            )
            if (score - movePenalty > bestScore) {
                bestLocation = newLocation
                bestCardToGrab = cardToGrab
                bestScore = score - movePenalty
            }
        }

        newLocation = nextDesk(newLocation)
        movePenalty += 1
    }
    return "MOVE $bestLocation ${if (player.grabDistance == 0) "" else "$bestCardToGrab"}"
}

fun findBestCardToGrab(
    player: Player,
    playerCards: StackOfCards,
    automaticPlayerCards: StackOfCards,
    restPlayerCards: StackOfCards,
    opponent: Player,
    totalOpponentCards: StackOfCards,
    applications: List<Application>,
    boardStack: StackOfCards
): Pair<Int, Int> {
    var bestCardToGrab = player.location
    var bestScore = -999
    for (n in 0 until nr_skills) {
        if (deskDistance(player.location, n) <= player.grabDistance) {
            val newPlayerCards = if (boardStack.cardsCount[n] > 0) playerCards.duplicateWithAdditionalCard(n) else playerCards.duplicateWithAdditionalCard(BONUS)
            val score = scorePlayerHand(player, newPlayerCards, automaticPlayerCards, restPlayerCards, opponent, totalOpponentCards, applications)
            System.err.println("at ${player.location} grabbing $n scores $score")

            if (score > bestScore) {
                bestCardToGrab = n
                bestScore = score
            }
        }
    }
    return Pair<Int, Int>(bestScore, bestCardToGrab)
}

//fun findBestActionWithPenalty(
//    player: Player,
//    playerCards: StackOfCards,
//    automaticPlayerCards: StackOfCards,
//    opponent: Player,
//    applications: List<Application>
//): Int {
//    return 0
//}

// Returns a positive number
fun allowedTechDebt(player: Player, totalPlayerCards: StackOfCards, opponent: Player, totalOpponentCards: StackOfCards): Int {
    if (player.busyWithLastApp) return 0
    return 1 +
            max(0, totalOpponentCards.technicalDebtCardsCount - totalPlayerCards.technicalDebtCardsCount) +
            max(0, opponent.score - player.score) * 2
}

fun scorePlayerHand(
    player: Player,
    playerCards: StackOfCards,
    automaticPlayerCards: StackOfCards,
    restPlayerCards: StackOfCards,
    opponent: Player,
    totalOpponentCards: StackOfCards,
    applications: List<Application>
): Int {
    val (_, releaseScore) = findBestApplicationToRelease(applications, playerCards + automaticPlayerCards)
    if (-releaseScore <= allowedTechDebt(player, playerCards + restPlayerCards, opponent, totalOpponentCards) ) {
        return 1000 + releaseScore // Super high score, we can release an app! Adding the tech debt will allow the score to be
                                   // lower, so a better possibility may be found later on
    }

    var scoring = listOf(
        10, // TRAINING = 0
        10, // CODING = 1
        if (player.wantsDailyRoutine) 50 else 10, // DAILY_ROUTINE = 2
        10, // TASK_PRIORITIZATION = 3
        if (player.wantsArchitectureStudy) 50 else 10, // ARCHITECTURE_STUDY = 4
        100, // CONTINUOUS_INTEGRATION = 5 We always want CI cards, so these are worth a lot of points
        20, // CODE_REVIEW = 6
        20, // REFACTORING = 7
        5, // BONUS = 8
        -5, //TECH_DEBT = 9
    )

    return playerCards.cardsCount.zip(scoring) {a, b -> a * b }.sum()
}

fun findDeskSkillOpportunityCommand(
        skill: Int,
        player: Player,
        opponentLocation: Int,
        cardsOnBoard: StackOfCards): String? {
    if (cardsOnBoard.cardCount(skill) <= 0) {
        return null
    }
    var startLocation = if(player.location == -1) nr_skills - 1 else player.location
    var newLocation = nextDesk(startLocation)
    while (newLocation != startLocation) {
        if (deskDistance(newLocation, opponentLocation) > 1 && deskDistance(newLocation, skill) <= player.grabDistance) {
            return "MOVE $newLocation ${if (player.grabDistance > 0) skill else ""}"
        }
        newLocation = nextDesk(newLocation)
    }
    return null
}

fun findNextAvailableDesk(
        playerLocation: Int,
        opponentLocation: Int): Int {
    var startLocation = if (playerLocation == -1) nr_skills - 1 else playerLocation
    var newLocation = nextDesk(startLocation)
    while (newLocation != startLocation) {
        if (deskDistance(newLocation, opponentLocation) > 1) {
            break
        }
        newLocation = nextDesk(newLocation)
    }
    assert(newLocation != startLocation)
    return newLocation // There is always a free spot, so this is never startLocation
}

fun main(args : Array<String>) {
    val input = Scanner(System.`in`)

    // game loop
    while (true) {
        val gamePhase = Phase(input.next()) // can be MOVE, GIVE_CARD, THROW_CARD, PLAY_CARD or RELEASE
        val applications = List<Application>(input.nextInt()) { Application.parse(input) }

        val player = Player.parse(input)
        val opponent = Player.parse(input)

        var playerCards = StackOfCards()
        var automatedPlayerCards = StackOfCards()
        var restPlayerCards = StackOfCards()
        var totalOpponentCards = StackOfCards()
        var boardStack = initial_board_stack
        val cardLocationsCount = input.nextInt()
        for (i in 0 until cardLocationsCount) {
            val cardsLocation = input.next()
            val stackOfCards = StackOfCards.parse(input)
            if (cardsLocation == "AUTOMATED") {
                automatedPlayerCards = stackOfCards
            }
            if (cardsLocation == "HAND") {
                playerCards = stackOfCards
            }
            if (cardsLocation == "DRAW" || cardsLocation == "DISCARD" || cardsLocation == "PLAYED_CARDS") {
                restPlayerCards += stackOfCards
            }
            if (cardsLocation == "OPPONENT_CARDS") {
                totalOpponentCards = stackOfCards
            }
            // Not used: OPPONENT_AUTOMATED
            boardStack -= stackOfCards
        }
        boardStack = boardStack - player - opponent

        val possibleMovesCount = input.nextInt()
        if (input.hasNextLine()) {
            input.nextLine()
        }
        val possibleMoves = List<String>(possibleMovesCount) { input.nextLine() }
        // In the first league: RANDOM | MOVE <zoneId> | RELEASE <applicationId> | WAIT;
        // In later leagues: | GIVE <cardType> | THROW <cardType> | TRAINING | CODING | DAILY_ROUTINE | TASK_PRIORITIZATION <cardTypeToThrow> <cardTypeToTake> | ARCHITECTURE_STUDY | CONTINUOUS_DELIVERY <cardTypeToAutomate> | CODE_REVIEW | REFACTORING;
        System.err.println(possibleMoves)

        // MAIN AI heuristics
        /////////////////////
        if (gamePhase.isMovePhase) {
            println(findBestMoveCommand(player, playerCards, automatedPlayerCards, restPlayerCards, opponent, totalOpponentCards, applications, boardStack))
        } else if (gamePhase.isThrowCardPhase) {
            // Note: location of the player is the moved-to location, even though the figure is drawn at desk 8.
            var possiblePlayerCards = playerCards.allDuplicatesOneCardLess()
            var bestScore = -999
            var bestPlayerCards = playerCards
            for (pCards in possiblePlayerCards) {
                val score = scorePlayerHand(player, pCards, automatedPlayerCards, restPlayerCards, opponent, totalOpponentCards, applications)
                if (score > bestScore) {
                    bestScore = score
                    bestPlayerCards = pCards
                }
            }
            for (n in 0 until nr_skills + 1) {
                if (bestPlayerCards.cardsCount[n] < playerCards.cardsCount[n]) {
                    println("THROW $n")
                    break
                }
            }
        } else if (gamePhase.isPlayPhase) {
            var playCommand: String? = null
            var (_, score) = findBestApplicationToRelease(applications, playerCards + automatedPlayerCards)
            val techDebtLimit = allowedTechDebt(player, playerCards + restPlayerCards, opponent, totalOpponentCards)
            if (score >= -techDebtLimit && (!player.busyWithLastApp || score == 0)) {
                // First goal: release applications. So play nothing if we can do that.
                playCommand = "WAIT"
            }
            if (playCommand == null) {
                // Secondary goal: build the deck. Priority is CI.
                if (possibleMoves.contains("CONTINUOUS_INTEGRATION 8")) {
                    playCommand = "CONTINUOUS_INTEGRATION 8"
                }
            }
            if (playCommand == null) {
                val possibleCICommands = possibleMoves.filter { it.startsWith("CONTINUOUS_INTEGRATION") }
                if (possibleCICommands.isNotEmpty()) {
                    // TODO: only do this if the applications actually need the skill
                    playCommand = possibleCICommands.random()
                }
            }
            if (playCommand == null && player.wantsDailyRoutine) {
                if (possibleMoves.contains("DAILY_ROUTINE")) {
                    playCommand = "DAILY_ROUTINE"
                }
            }
            if (playCommand == null && player.wantsArchitectureStudy) {
                if (possibleMoves.contains("ARCHITECTURE_STUDY")) {
                    playCommand = "ARCHITECTURE_STUDY"
                }
            }
            if (playCommand == null && possibleMoves.contains("CODE_REVIEW")) {
                playCommand = "CODE_REVIEW"
            }
            if (playCommand == null && possibleMoves.contains("REFACTORING")) {
                playCommand = "REFACTORING"
            }
            if (playCommand == null && boardStack.cardCount(CONTINUOUS_INTEGRATION) > 0) {
                val possibleTPCommands = possibleMoves.filter { it.startsWith("TASK_PRIORITIZATION") && it.endsWith("5") }
                if (possibleTPCommands.isNotEmpty()) {
                    playCommand = possibleTPCommands.random()
                }
            }
            println(playCommand ?: "WAIT")
        } else if (gamePhase.isReleasePhase) {
            var (app, score) = findBestApplicationToRelease(applications, playerCards + automatedPlayerCards)
            val techDebtLimit = allowedTechDebt(player, playerCards + restPlayerCards, opponent, totalOpponentCards)
            if (app != null && score >= -techDebtLimit && (!player.busyWithLastApp || score == 0)) {
                val releaseComment = when (score) {
                    0 -> "perfect!"
                    else -> ""
                }
                println("RELEASE ${app.id} $releaseComment")
            } else {
                println("WAIT")
            }
        } else {
            // Give phase, but we never do that.
            println("RANDOM")
        }
    }
}